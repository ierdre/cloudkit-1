//
//  ViewController.swift
//  testCloudKit
//
//  Created by Thibaut LE LEVIER on 14/06/2019.
//  Copyright © 2019 Cogelec. All rights reserved.
//

import UIKit
import CloudKit
class ViewController: UIViewController {
 let myContainer = "iCloud.hubosoft.testTutorial"
    
    @IBAction func addIcloudReccord(_ sender: Any) {
       /* let containerIdentifier = "iCloud.hubosoft.testTutorial"
        let secondContainer = CKContainer(identifier: containerIdentifier)
        
        let record = CKRecord(recordType: "TESTCloud")
        record["monTitre"] = "** ----> date = \(Date())" as CKRecordValue
        print(record)
        secondContainer.publicCloudDatabase.save(record, completionHandler: {rec, error in
            print (" --> \(rec ) \(error)" )
        })*/
        let containerIdentifier = myContainer
        let secondContainer = CKContainer(identifier: containerIdentifier)
        let ckRecordZoneID = CKRecordZone.ID(zoneName: "myZone", ownerName: CKOwnerDefaultName)
        
        //Id unique identification
        let ckRecordID = CKRecord.ID(recordName: " name_test", zoneID: ckRecordZoneID)
        
        // let record = CKShare(rootRecord: "repas", shareID: ckRecordID)
        
        let record = CKRecord(recordType: "repas", recordID: ckRecordID)
        // let record = CKRecord(recordType: "repas")
        // record["date"] = Date() as Date
        // record["name"] = " TEST BCD" as CKRecordValue
        //let share = CKShare(rootRecord: record, shareID: ckRecordID)
        
        //print(record)
        let customZone = CKRecordZone(zoneName: "myZone")
      
        
        let controller = UICloudSharingController { controller,
            preparationCompletionHandler in
            
            let share = CKShare(rootRecord: record)
            share["title"] = " My First Share" as CKRecordValue
            share.publicPermission = .readOnly
            
            let modifyRecordsOperation = CKModifyRecordsOperation(
                recordsToSave: [record, share],
                recordIDsToDelete: nil)
            
            modifyRecordsOperation.timeoutIntervalForRequest = 10
            modifyRecordsOperation.timeoutIntervalForResource = 10
            
            modifyRecordsOperation.modifyRecordsCompletionBlock = { records,
                recordIDs, error in
                if error != nil {
                    print(error?.localizedDescription)
                }
                preparationCompletionHandler(share,
                                             CKContainer.default(), error)
            }
            secondContainer.privateCloudDatabase.add(modifyRecordsOperation)
        }
        
        controller.availablePermissions = [.allowPublic, .allowReadOnly]
        //controller.popoverPresentationController?.barButtonItem =
        //  shareButton as? UIBarButtonItem
        
        present(controller, animated: true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

